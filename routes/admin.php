<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('admin/login', 'CustomAuth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'CustomAuth\AdminLoginController@login')->name('admin.login');
Route::get('admin/logout', 'CustomAuth\AdminLoginController@logout')->name('admin.logout');

Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function ($router) {
    $router->get('dashboard', 'Admin\Dashboard\DashboardController@index')->name('admin.dashboard');

});