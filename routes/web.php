<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
include 'admin.php';

Route::get('/', function () {
    return view('frontend.index');
});


Route::get('/homepage', 'FrontendController@homepage')->name('homepage');
Route::get('/joblist', 'FrontendController@joblist')->name('joblist');
Route::get('/jobdetail','FrontendController@jobdetail')->name('jobdetail');

//Route::get('/home', 'HomeController@index')->name('home');
