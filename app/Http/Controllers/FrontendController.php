<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function homepage ()
    {
    	return view ('frontend.index');
    }
    public function joblist()
    {
    	return view ('frontend.jobs.job-list');
    }
    public function jobdetail()
    {
    	return view ('frontend.jobs.job-detail');
    }
}
