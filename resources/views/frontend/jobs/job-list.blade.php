@extends('frontend.layouts.app')

<!-- Start Slider -->
@section('content')
<!-- Start Navigation -->
			<nav class="navbar navbar-default navbar-fixed navbar-light white bootsnav">

				<div class="container">            
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
					<!-- Start Header Navigation -->
					<div class="navbar-header">
						<a class="navbar-brand" href="{{route('homepage')}}"><img src="{{asset('assets/img/job_portal/logo.jpeg')}}" class="logo logo-display" alt="" style="height: 55px;
    width: 15%;"><img src="{{asset('assets/img/job_portal/logo.jpeg')}}" class="logo logo-scrolled" alt="" style="    height: 55px;
    width: 14%;"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-menu">
						<ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
							
							<li class="dropdown">
								<a href="{{route('homepage')}}" class="dropdown-toggle" data-toggle="dropdown">Home</a>
								
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Jobs</a>
								<ul class="dropdown-menu animated fadeOutUp">
									
									<li class="dropdown">
										<a href="{{route('joblist')}}" class="dropdown-toggle" data-toggle="dropdown">Job List</a>
										
										
									</li>
									
									<li class="dropdown">
										<a href="{{route('jobdetail')}}" class="dropdown-toggle" data-toggle="dropdown">Job Detail</a>
										
									</li>
									
																	
								</ul>
							</li>
							
							<li class="dropdown megamenu-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Brows</a>
								
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
							<li><a href="login.html"><i class="fa fa-pencil" aria-hidden="true"></i>SignIn</a></li>
							<li class="left-br"><a href="javascript:void(0)"  data-toggle="modal" data-target="#signup" class="signin">Sign In Now</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>   
			</nav>
			<!-- End Navigation -->
			<div class="clearfix"></div>
			
			<!-- Title Header Start -->
			<section class="inner-header-title" style="background-image:url(assets/img/banner-10.jpg);">
				<div class="container">
					<h1>Browse Jobs</h1>
				</div>
			</section>
			<div class="clearfix"></div>
			<!-- Title Header End -->
			
			<!-- ========== Begin: Brows job ===============  -->
			<section>
				<div class="container">
					<!-- Company Searrch Filter Start -->
					<div class="row extra-mrg">
						<div class="wrap-search-filter">
							<form>
								<div class="col-md-4 col-sm-4">
									<input type="text" class="form-control" placeholder="Keyword: Name, Tag">
								</div>
								<div class="col-md-3 col-sm-3">
									<input type="text" class="form-control" placeholder="Location: City, State, Zip">
								</div>
								<div class="col-md-3 col-sm-3">
									<select class="form-control" id="j-category">
										<option value="">&nbsp;</option>
										<option value="1">Information Technology</option>
										<option value="2">Mechanical</option>
										<option value="3">Hardware</option>
										<option value="4">Hospitality & Tourism</option>
										<option value="5">Education & Training</option>
										<option value="6">Government & Public</option>
										<option value="7">Architecture</option>
									</select>

								</div>
								<div class="col-md-2 col-sm-2">
									<button type="submit" class="btn btn-primary full-width">Filter</button>
								</div>
							</form>
						</div>
					</div>
					<!-- Company Searrch Filter End -->
					
					<!--Browse Job In Grid-->
					<div class="row extra-mrg">
					
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-1.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="full-time">Full Time</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
								<span class="tg-themetag tg-featuretag">Premium</span>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-2.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="part-time">Part Time</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-3.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="freelanc">Freelancer</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
								<span class="tg-themetag tg-featuretag">Premium</span>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-4.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="enternship">Enternship</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-5.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="full-time">Full Time</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-6.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="part-time">Part Time</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
								<span class="tg-themetag tg-featuretag">Premium</span>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-6.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="full-time">Full Time</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-7.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="freelanc">Freelancer</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
							</div>
						</div>
						
						<div class="col-md-4 col-sm-6">
							<div class="grid-view brows-job-list">
								<div class="brows-job-company-img">
									<img src="assets/img/com-1.jpg" class="img-responsive" alt="" />
								</div>
								<div class="brows-job-position">
									<h3><a href="job-detail.html">Web Developer</a></h3>
									<p><span>Google</span></p>
								</div>
								<div class="job-position">
									<span class="job-num">5 Position</span>
								</div>
								<div class="brows-job-type">
									<span class="enternship">Enternship</span>
								</div>
								<ul class="grid-view-caption">
									<li>
										<div class="brows-job-location">
											<p><i class="fa fa-map-marker"></i>QBL Park, C40</p>
										</div>
									</li>
									<li>
										<p><span class="brows-job-sallery"><i class="fa fa-money"></i>$110 - 200</span></p>
									</li>
								</ul>
							</div>
						</div>
						
					</div>
					<!--/.Browse Job In Grid-->

					<div class="row">
						<ul class="pagination">
							<li><a href="#"><i class="ti-arrow-left"></i></a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li> 
							<li><a href="#">4</a></li> 
							<li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li> 
							<li><a href="#"><i class="ti-arrow-right"></i></a></li> 
						</ul>
					</div>
					
				</div>
			</section>
			<!-- ========== Begin: Brows job Grid End ===============  -->
			
			

@endsection