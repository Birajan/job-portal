@extends('frontend.layouts.app')

<!-- Start Slider -->
@section('content')
<!-- Start Navigation -->
			<nav class="navbar navbar-default navbar-fixed navbar-light white bootsnav">

				<div class="container">            
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
						<i class="fa fa-bars"></i>
					</button>
					<!-- Start Header Navigation -->
					<div class="navbar-header">
						<a class="navbar-brand" href="{{route('homepage')}}"><img src="{{asset('assets/img/job_portal/logo.jpeg')}}" class="logo logo-display" alt="" style="height: 55px;
    width: 15%;"><img src="{{asset('assets/img/job_portal/logo.jpeg')}}" class="logo logo-scrolled" alt="" style="    height: 55px;
    width: 14%;"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-menu">
						<ul class="nav navbar-nav navbar-left" data-in="fadeInDown" data-out="fadeOutUp">
							
							<li class="dropdown">
								<a href="{{route('homepage')}}" class="dropdown-toggle" data-toggle="dropdown">Home</a>
								
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Jobs</a>
								<ul class="dropdown-menu animated fadeOutUp">
									
									<li class="dropdown">
										<a href="{{route('joblist')}}" class="dropdown-toggle" data-toggle="dropdown">Job List</a>
										
										
									</li>
									
									<li class="dropdown">
										<a href="{{route('jobdetail')}}" class="dropdown-toggle" data-toggle="dropdown">Job Detail</a>
										
									</li>
									
																	
								</ul>
							</li>
							
							<li class="dropdown megamenu-fw"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Brows</a>
								
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">
							<li><a href="login.html"><i class="fa fa-pencil" aria-hidden="true"></i>SignIn</a></li>
							<li class="left-br"><a href="javascript:void(0)"  data-toggle="modal" data-target="#signup" class="signin">Sign In Now</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>   
			</nav>
<!-- Title Header Start -->
			<section class="inner-header-page">
				<div class="container">
					
					<div class="col-md-8">
						<div class="left-side-container">
							<div class="freelance-image"><a href="company-detail.html"><img src="assets/img/com-2.jpg" class="img-responsive" alt=""></a></div>
							<div class="header-details">
								<h4>Front End Developer</h4>
								<p>Google</p>
								<ul>
									<li><a href="http://themezhub.com/"><i class="fa fa-user"></i> 7 Vacancy</a></li>
									<li>
										<div class="star-rating" data-rating="4.2">
											<span class="fa fa-star fill"></span>
											<span class="fa fa-star fill"></span>
											<span class="fa fa-star fill"></span>
											<span class="fa fa-star fill"></span>
											<span class="fa fa-star-half fill"></span>
										</div>
									</li>
									<li><img class="flag" src="assets/img/gb.svg" alt=""> United Kingdom</li>
									<li><div class="verified-action">Verified</div></li>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="col-md-4 bl-1 br-gary">
						<div class="right-side-detail">
							<ul>
								<li><span class="detail-info">Availability</span>Full Time</li>
								<li><span class="detail-info">Experience</span>5 Year</li>
								<li><span class="detail-info">Age</span>22+ Year</li>
							</ul>
							<ul class="social-info">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</section>
			<div class="clearfix"></div>
			<!-- Title Header End -->
			
			<!-- Job Detail Start -->
			<section>
				<div class="container">
					
					<div class="col-md-8 col-sm-12">
						<div class="container-detail-box">
						
							<div class="apply-job-header">
								<h4>Front End Developer</h4>
								<a href="company-detail.html" class="cl-success"><span><i class="fa fa-building"></i>Google</span></a>
								<span><i class="fa fa-map-marker"></i>United Kingdom</span>
							</div>
							
							<div class="apply-job-detail">
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
								<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus.</p>
							</div>
							
							<div class="apply-job-detail">
								<h5>Skills</h5>
								<ul class="skills">
									<li>Css3</li>
									<li>Html5</li>
									<li>Photoshop</li>
									<li>Wordpress</li>
									<li>PHP</li>
									<li>Java Script</li>
								</ul>
							</div>
							
							<div class="apply-job-detail">
								<h5>Requirements</h5>
								<ul class="job-requirements">
									<li><span>Availability</span> Hourly</li>
									<li><span>Education</span> Graduate</li>
									<li><span>Age</span> 25+</li>
									<li><span>Experience</span> Intermidiate (3 - 5Year)</li>
									<li><span>Language</span> English, Hindi</li>
								</ul>
							</div>
							
							<a href="#" class="btn btn-success">Apply For This Job</a>
							
						</div>
						
						<div class="job-detail-statistic flex-middle-sm">
				
							<div class="statistic-item flex-middle">
								<div class="icon text-theme">
								<i class="ti-headphone theme-cl"></i></div>
								<span class="text">+91 215 245 6584</span>
							</div>

							<div class="statistic-item flex-middle">
								<div class="icon text-theme">
								<i class="ti-email theme-cl"></i></div>
								<span class="text"><a href="https://codeminifier.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="7f15101d0c0b101c143f18121e1613511c1012">[email&#160;protected]</a></span>
							</div>

							<div class="statistic-item flex-middle">
								<div class="icon text-theme">
								<i class="ti-skype theme-cl"></i></div>
								<span class="text">themezhub</span>
							</div>

						</div>
						
						<!-- Similar Jobs -->
						<div class="container-detail-box">
						
							<div class="row">
								<div class="col-md-12">
									<h4>Similar Jobs</h4>
								</div>
							</div>
							
							<div class="row">
								<div class="grid-slide-2">
									
									<!-- Single Freelancer & Premium job -->
									<div class="freelance-box">
										<div class="popular-jobs-container">
											<div class="popular-jobs-box">
												<span class="popular-jobs-status bg-success">hourly</span>
												<h4 class="flc-rate">$17/hr</h4>
												<div class="popular-jobs-box">
													<div class="popular-jobs-box-detail">
														<h4>Google Inc</h4>
														<span class="desination">Software Designer</span>
													</div>
												</div>
												<div class="popular-jobs-box-extra">
													<ul>
														<li>Php</li>
														<li>Android</li>
														<li>Html</li>
														<li class="more-skill bg-primary">+3</li>
													</ul>
													<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
												</div>
											</div>
											<a href="http://themezhub.com/" class="btn btn-popular-jobs bt-1">View Detail</a>
										</div>
									</div>
									
									<!-- Single Freelancer & Premium job -->
									<div class="freelance-box">
										<div class="popular-jobs-container">
											<div class="popular-jobs-box">
												<span class="popular-jobs-status bg-warning">Monthly</span>
												<h4 class="flc-rate">$570/Mo</h4>
												<div class="popular-jobs-box">
													<div class="popular-jobs-box-detail">
														<h4>Duff Beer</h4>
														<span class="desination">Marketting</span>
													</div>
												</div>
												<div class="popular-jobs-box-extra">
													<ul>
														<li>Php</li>
														<li>Android</li>
														<li>Html</li>
														<li class="more-skill bg-primary">+3</li>
													</ul>
													<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
												</div>
											</div>
											<a href="http://themezhub.com/" class="btn btn-popular-jobs bt-1">View Detail</a>
										</div>
									</div>
									
									<!-- Single Freelancer & Premium job -->
									<div class="freelance-box">
										<div class="popular-jobs-container">
											<div class="popular-jobs-box">
												<span class="popular-jobs-status bg-info">Weekly</span>
												<h4 class="flc-rate">$200/We</h4>
												<div class="popular-jobs-box">
													<div class="popular-jobs-box-detail">
														<h4>Cyberdyne Systems</h4>
														<span class="desination">Human Resource</span>
													</div>
												</div>
												<div class="popular-jobs-box-extra">
													<ul>
														<li>Php</li>
														<li>Android</li>
														<li>Html</li>
														<li class="more-skill bg-primary">+3</li>
													</ul>
													<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
												</div>
											</div>
											<a href="http://themezhub.com/" class="btn btn-popular-jobs bt-1">View Detail</a>
										</div>
									</div>
									
									<!-- Single Freelancer & Premium job -->
									<div class="freelance-box">
										<div class="popular-jobs-container">
											<div class="popular-jobs-box">
												<span class="popular-jobs-status bg-danger">Yearly</span>
												<h4 class="flc-rate">$2000/Pa</h4>
												<div class="popular-jobs-box">
													<div class="popular-jobs-box-detail">
														<h4>Wayne Enterprises</h4>
														<span class="desination">App Designer</span>
													</div>
												</div>
												<div class="popular-jobs-box-extra">
													<ul>
														<li>Php</li>
														<li>Android</li>
														<li>Html</li>
														<li class="more-skill bg-primary">+3</li>
													</ul>
													<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui.</p>
												</div>
											</div>
											<a href="http://themezhub.com/" class="btn btn-popular-jobs bt-1">View Detail</a>
										</div>
									</div>
							
								</div>
							</div>
							
						</div>
					</div>
					
					<!-- Sidebar Start-->
					<div class="col-md-4 col-sm-12">
						
						<!-- Job Detail -->
						<div class="sidebar-container">
							<div class="sidebar-box">
								<span class="sidebar-status">Full Time</span>
								<h4 class="flc-rate">20K - 30K</h4>
								<div class="sidebar-inner-box">
									<div class="sidebar-box-thumb">
										<img src="assets/img/com-2.jpg" class="img-responsive" alt="" />
									</div>
									<div class="sidebar-box-detail">
										<h4>Google Info</h4>
										<span class="desination">App Designer</span>
									</div>
								</div>
								<div class="sidebar-box-extra">
									<ul>
										<li>Php</li>
										<li>Android</li>
										<li>Html</li>
										<li class="more-skill bg-primary">+3</li>
									</ul>
									<ul class="status-detail">
										<li class="br-1"><strong>Canada</strong>Location</li>
										<li class="br-1"><strong>748</strong>View</li>
										<li><strong>03</strong>Post</li>
									</ul>
								</div>
							</div>
							<a href="#" class="btn btn-sidebar bt-1 bg-success">Apply For This</a>
						</div>
						
						<!-- Share This Job -->
						<div class="sidebar-wrapper">
							<div class="sidebar-box-header bb-1">
								<h4>Share This Job</h4>
							</div>
						
							<ul class="social-share">
								<li><a href="#" class="fb-share"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#" class="tw-share"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#" class="gp-share"><i class="fa fa-google-plus"></i></a></li>
								<li><a href="#" class="in-share"><i class="fa fa-instagram"></i></a></li>
								<li><a href="#" class="li-share"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#" class="be-share"><i class="fa fa-behance"></i></a></li>
							</ul>
						</div>
						
					</div>
					<!-- End Sidebar -->
					
				</div>
			</section>
			<!-- Job Detail End -->
						
			
			

@endsection