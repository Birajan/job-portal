<!-- ============================ Before Footer ================================== -->
<div class="before-footer">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                <div class="jb4-form-fields">
                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="Enter your email address">
                        <span class="input-group-btn">
									<button class="btn theme-bg" type="submit"><span class="fa fa-paper-plane-o"></span></button>
								  </span>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 hill">
                <ul class="job stock-facts">
                    <li><span>2744</span></br>Jobs Posted</li>
                    <li><span>2365</span></br>Jobs Posted</li>
                    <li><span>2021</span></br>Freelancer</li>
                    <li><span>7542</span></br>Companies</li>
                </ul>
            </div>

        </div>
    </div>
</div>
<!-- ============================ Before Footer ================================== -->

<!-- ============================ Footer Start ================================== -->
<footer class="dark-footer skin-dark-footer">
    <div>
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-3">
                    <div class="footer-widget">
                        <img src="{{asset('assets/img/job_portal/logo.jpeg')}}" class="img-footer" alt="" />
                        <div class="footer-add">
                            <p>Kathmandu</br>Nepal</p>
                            <p><strong>Email:</strong></br><a href="https://codeminifier.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="87efe2ebebe8c7ede8e5f4f3e8e4eca9e4e8ea">[email&#160;protected]</a></p>
                            <p><strong>Call:</strong></br>+977 1234567</p>
                        </div>

                    </div>
                </div>
                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget">
                        <h4 class="widget-title">Navigations</h4>
                        <ul class="footer-menu">
                            <li><a href="http://themezhub.com/">New Home Design</a></li>
                            <li><a href="browse-candidate-list.html">Browse Candidates</a></li>
                            <li><a href="browse-employer-list.html">Browse Employers</a></li>
                            <li><a href="advance-search-2.html">Advance Search</a></li>
                            <li><a href="http://themezhub.com/">Job With Map</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget">
                        <h4 class="widget-title">The Highlights</h4>
                        <ul class="footer-menu">
                            <li><a href="index-2.html">Home Page 2</a></li>
                            <li><a href="index-3.html">Home Page 3</a></li>
                            <li><a href="index-4.html">Home Page 4</a></li>
                            <li><a href="index-5.html">Home Page 5</a></li>
                            <li><a href="login.html">LogIn</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2">
                    <div class="footer-widget">
                        <h4 class="widget-title">My Account</h4>
                        <ul class="footer-menu">
                            <li><a href="candidate-dashboard.html">Dashboard</a></li>
                            <li><a href="applications.html">Applications</a></li>
                            <li><a href="packages.html">Packages</a></li>
                            <li><a href="candidate-resume.html">resume.html</a></li>
                            <li><a href="register.html">SignUp Page</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3">
                    <div class="footer-widget">
                        <h4 class="widget-title">Download Apps</h4>
                        <a href="#" class="other-store-link">
                            <div class="other-store-app">
                                <div class="os-app-icon">
                                    <i class="ti-android theme-cl"></i>
                                </div>
                                <div class="os-app-caps">
                                    Google Play
                                    <span>Get It Now</span>
                                </div>
                            </div>
                        </a>
                        <a href="#" class="other-store-link">
                            <div class="other-store-app">
                                <div class="os-app-icon">
                                    <i class="ti-apple theme-cl"></i>
                                </div>
                                <div class="os-app-caps">
                                    App Store
                                    <span>Now it Available</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6 col-md-6">
                    <p class="mb-0">© 2020 Legends Zone. Designd By <a href="#">AccessWorld  Tech</a> All Rights Reserved</p>
                </div>

                <div class="col-lg-6 col-md-6 text-right">
                    <ul class="footer-bottom-social">
                        <li><a href="#"><i class="ti-facebook"></i></a></li>
                        <li><a href="#"><i class="ti-twitter"></i></a></li>
                        <li><a href="#"><i class="ti-instagram"></i></a></li>
                        <li><a href="#"><i class="ti-linkedin"></i></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>
<!-- ============================ Footer End ================================== -->