<!doctype html>
<html lang="en">


<!-- Mirrored from codeminifier.com/job-stock-v5.3/job-stock/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 02 Apr 2020 12:39:53 GMT -->
<head>
    <!-- Basic Page Needs==================================================-->
    <title>Job Stock - Responsive Job Portal Bootstrap Template | ThemezHub</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS==================================================-->
    <link rel="stylesheet" href="{{asset('assets/plugins/css/plugins.css')}}">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" id="jssDefault" href="{{asset('assets/css/colors/green-style.css')}}"> </head>

<body>
<div class="Loader"></div>
<div class="wrapper">

    @include('frontend.layouts.partials.header')

        @yield('content')

        @include('frontend.layouts.partials.footer')


    <!-- Scripts
			================================================== -->
    <script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript" src="{{asset('assets/plugins/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/viewportchecker.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/bootsnav.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/wysihtml5-0.3.0.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/bootstrap-wysihtml5.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/datedropper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/loader.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/gmap3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/plugins/js/jquery.easy-autocomplete.min.js')}}"></script>
    <!-- Custom Js -->
    <script src="{{asset('assets/js/custom.js')}}"></script><script type="text/javascript" src="{{asset('assets/plugins/js/counterup.min.js')}}"></script>
    <script src="{{asset('assets/js/jQuery.style.switcher.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#styleOptions').styleSwitcher();
        });
    </script>
    <script>
        function openRightMenu() {
            document.getElementById("rightMenu").style.display = "block";
        }

        function closeRightMenu() {
            document.getElementById("rightMenu").style.display = "none";
        }
    </script>
</div>
</body>
</html>