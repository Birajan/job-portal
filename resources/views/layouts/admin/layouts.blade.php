<!DOCTYPE html>
<html lang="en">
@include('layouts.admin.head')
@if (auth()->guest())
    <body class="menubar-hoverable header-fixed ">

    @yield('guest')

    </body>
@else
    <body class="menubar-hoverable header-fixed menubar-pin">

    @include('layouts.admin.header')
    <!-- BEGIN BASE-->
    <div id="base">
        <!-- BEGIN CONTENT-->
        <div id="content">
            @yield('content')
        </div>
        <!-- END CONTENT-->
        @include('layouts.admin.sidebar')
    </div><!--end #base-->
    <!-- END BASE -->
    @endif
    @include('layouts.admin.footer')
    </body>
</html>
