<script src="{{asset('admin/js/libs/jquery/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('admin/js/libs/jquery/jquery-migrate-1.2.1.min.js')}}"></script>
<script src="{{asset('admin/js/libs/bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/libs/spin.js/spin.min.js')}}"></script>
<script src="{{asset('admin/js/libs/autosize/jquery.autosize.min.js')}}"></script>
<script src="{{asset('admin/js/libs/nanoscroller/jquery.nanoscroller.min.js')}}"></script>
<script src="{{asset('admin/js/core/source/App.js')}}"></script>
<script src="{{asset('admin/js/app.js')}}"></script>
<script src="{{asset('admin/js/core/source/AppNavigation.js')}}"></script>
<script src="{{asset('admin/js/core/source/AppCard.js')}}"></script>
<script src="{{asset('admin/js/core/source/AppForm.js')}}"></script>
<script src="{{asset('admin/js/core/source/AppNavSearch.js')}}"></script>
<script src="{{asset('admin/js/core/source/AppVendor.js')}}"></script>
<script src="{{asset('admin/js/core/demo/Demo.js')}}"></script>
<script src="{{asset('admin/js/ckeditor/ckeditor.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="{{asset('admin/js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{asset('admin/js/core/demo/DemoPageContacts.js') }}"></script>
<script src="{{ asset('admin/js/libs/bootbox/bootbox.min.js') }}"></script>
<script src="{{ asset('admin/js/core/source/AppBootBox.min.js') }}"></script>

{{--{!! Toastr::render() !!}--}}

@yield('page-specific-scripts')
