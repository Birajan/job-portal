@extends('layouts.admin.layouts')

@section('title','Login')

@section('page-specific-styles')
    <style type="text/css">
        .logo {
            margin-top: 80px;
            margin-bottom: 15px;
        }

    </style>
@endsection

@section('guest')

    <!-- BEGIN LOGIN SECTION -->
    <section class="section-account">
        <div class="img-backdrop"></div>
        <div class="spacer"></div>
        <div class="card contain-sm style-transparent">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-8">
                        <br/>
                        <span class="text-lg text-bold text-primary">Job Portal</span>
                        <br/><br/>
                        <form class="form form-validate"  accept-charset="utf-8" method="POST"
                              action="{{ route('admin.login') }}" autocomplete="off" novalidate>
                            @csrf
                            <div class="form-group">
                                <input type="text" class="form-control{{ $errors->has('name') || $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="login" value="{{ old('name') ?: old('email') }}" required>
                                <label for="login">Username/Email</label>
                                @if ($errors->has('name') || $errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
							<p>{{ $errors->first('name') ?: $errors->first('email') }}</p>
						</span>
                                @endif
                            </div>
                            <div class="form-group">
                                <input type="password"
                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password"
                                       name="password" required>
                                <label for="password">Password</label>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
							<p>{{ $errors->first('password') }}</p>
						</span>
                                @endif
                                <p class="help-block"><a href="#">Forgotten?</a></p>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-xs-6 text-left">
                                    <button class="btn btn-primary btn-raised" type="submit">Login</button>
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </form>
                    </div><!--end .col -->
                </div><!--end .row -->
            </div><!--end .card-body -->
        </div><!--end .card -->
    </section>
    <!-- END LOGIN SECTION -->

@endsection

@section('page-specific-scripts')
    <script src="{{ asset('admin/js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('admin/js/libs/jquery-validation/dist/additional-methods.min.js') }}"></script>
@endsection
